﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.IO;
using System.Net.Mime;
using System.Net.WebSockets;
using System.Reflection;
using System.Security.Cryptography;
using System.Threading;
using Microsoft.VisualBasic;

namespace Veeam
{
    class VeeamSHA256Signature_2
    {
        private static BinaryReader _reader;
        private static int _lineNum;
        private static int _blockSize;
        private static long _blockNum;
        private static int _tallSize;

        private static int _finishedThreads = 0;

        private static readonly object readerBlock = new object();
        private static readonly object iterBlock = new object();

        private static void HashCalc(int lineNum, byte[] byteArray)
        {
            using (var sha256 = new SHA256Managed())
            {
                var arrayHash = sha256.ComputeHash(byteArray);

                Console.WriteLine($"{lineNum}:{BitConverter.ToString(arrayHash).Replace("-", string.Empty)}");

            }
        }

        private static void ThreadProcess()
        {
            int tLineNum = -1;
            var buffer = new byte[_blockSize];

            while (true)
            {
                lock (readerBlock)
                {
                    if (_blockNum > 0)
                    {
                        _blockNum--;
                        tLineNum = _lineNum;
                        _lineNum++;

                        _reader.Read(buffer);
                    }
                    else
                    {
                        if (_tallSize > 0)
                        {
                            Array.Resize(ref buffer, _tallSize);
                            _tallSize = 0;
                            tLineNum = _lineNum;

                            _reader.Read(buffer);
                        }
                        else
                        {
                            break;
                        }
                    }
                    
                }

                HashCalc(tLineNum, buffer);
            }

            lock (iterBlock) _finishedThreads++;
            Thread.CurrentThread.Join();
        }

        public static void Start(string filePath, int blockSize)
        {
            _lineNum = 0;

            var process = Process.GetCurrentProcess();
            
            int threadCount = Environment.ProcessorCount;
            while (threadCount * blockSize > Process.GetCurrentProcess().WorkingSet64)
            {
                threadCount--;
            }

            if (threadCount == 0) throw new Exception("Not enough memory for start");

            var fileInfo = new FileInfo(filePath);
            _blockNum = fileInfo.Length / blockSize;
            _tallSize = (int)(fileInfo.Length - blockSize * _blockNum);

            _blockSize = blockSize;
            try
            {
                using (var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                {
                    _reader = new BinaryReader(fileStream);

                    for (int i = 0; i < threadCount; i++)
                    {
                        var thread = new Thread(x => ThreadProcess())
                        {
                            IsBackground = true
                        };
                        thread.Start();
                    }

                    while (_finishedThreads != threadCount)
                    {
                        var foo = Process.GetCurrentProcess().Threads.Count;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"An error was detected in the process: {e.Message}\n{e.StackTrace}");
            }

        }
    }

    class Program
    {
        private static string HELP_ARGS = "Usage: 'blockSize' 'filePath'";

        static void Main(string[] args)
        {
            int blockSize;
            string filePath;

            if (args.Length != 2) 
                Console.WriteLine(HELP_ARGS);
            else
                try
                {
                    if (!int.TryParse(args[0], out blockSize))
                        throw new Exception("Wrong format blockSize");
                    if (!File.Exists(args[1]))
                        throw new Exception("File doesn't exists");
                    filePath = args[1];

                    VeeamSHA256Signature_2.Start(filePath, blockSize);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"An error was detected in the process: {e.Message}\n{e.StackTrace}");
                }

            //int blockSize = 1024 * 1024;
            // = 64;
            //string filePath = "E:/Torrent/Venom.2018.BDRip.1080p.60fps.GTP.mkv";
            // = "E:/git/Veeam/Veeam/Veeam/test.txt";
            
            
        }
    }
}
